# Deep Learning Illustrated

Sample code for "Deep Learning Illustrated: A Visual, Interactive Guide to Artificial Intelligence" book from O'Reilly Learning.

## Setup

Simplest way to get all dependencies is to install `jonkrohn/deep-learning-illustrated` Docker image:

    docker pull jonkrohn/deep-learning-illustrated:book

Then just run this Docker image. Simplest way to run it is to use Docker Dashboard or Kitematic.

Jupyter is run on port `8888` in the docker container, so port forwarding need to be added. Simplest is to map it to same port on localhost.

**Note** By default jupyter saves notebooks into local directory and in case of Docker it will be stored in docker virtual drive, which will be reset after image will be shutdown.

To save a notebook to a location outside of Docker container use "File" -> "Download as" -> "Notebook (.ipynb)" option from the notebook menu.

Other solution to keep notebooks outside of docker container is to mount external volume. First need to create the volume, in the docker cli execute following command:

    docker volume create docker-notebooks

Need to check that the directory exists, if not create it:

    mkdir docker-notebooks

Then run docker with the volume to mount:

    docker run -d -v docker-notebooks:/home/jovyan -p 8888:8888 -p 6006:6006 --name dli-stack jonkrohn/deep-learning-illustrated:book
